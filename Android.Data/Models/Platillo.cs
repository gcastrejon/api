﻿namespace Android.Data.Models
{
    public class Platillo
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public string? Imagen { get; set; }

        public PlatilloDTO ToDTO()
        {
            return new PlatilloDTO()
            {
                Id = Id,
                Imagen = Imagen,
                Nombre = Nombre
            };
        }
    }
}
