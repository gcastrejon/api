using Android.Data.Models;

namespace Android.Data
{
    public class PlatilloDTO
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public string? Imagen { get; set; }

        public Platillo ToModel()
        {
            return new Platillo()
            {
                Id = Id,
                Nombre = Nombre,
                Imagen = Imagen
            };
        }
    }
}