﻿using Android.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Android.Data.Database
{
    public class RestauranteDBContext: DbContext
    {
        public DbSet<Platillo>? Platillos { get; set; }


        public DbSet<Ingrediente>? Ingredientes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            var connectionString = @"Server=localhost;Port=3306;Database=db_self_menu_pruebas;Uid=root;Pwd=12345678;";
            builder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            //builder.UseSqlServer(@"Server=tcp:practicas-rafa.database.windows.net,1433;Initial Catalog=Restaurante;Persist Security Info=False;User ID=gcastrejon;Password=Practicas@2021;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            //builder.UseSqlServer(@"Data Source=XPS-GABRIEL;Initial Catalog=Restaurante;Integrated Security=True;TrustServerCertificate=True");
        }
    }
}
