using Android.Data;
using Android.Web.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace Android.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class PlatillosController : ControllerBase
    {
        private readonly IRestauranteRepository Repository;

        public PlatillosController(IRestauranteRepository repository)
        {
            Repository = repository;
        }

        [HttpGet]
        //public async Task<IActionResult> Get()
        public IActionResult Get()
        {
            //var respuesta = await Repository.GetPlatillos();
            var respuesta = Repository.GetPlatillos();
            return Ok(respuesta);
        }

        /*[HttpPost]
        public IActionResult Post(PlatilloDTO platillo)
        {
            var response = false;
            if (platillo != null)
            {
                response = Repository.AddPlatillo(platillo);
            }
            return Ok(response);
        }*/

    }
}