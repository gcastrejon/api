﻿using Android.Data;

namespace Android.Web.Contracts
{
    public interface IRestauranteRepository
    {
        IList<PlatilloDTO> GetPlatillos();
        //Task<IList<PlatilloDTO>> GetPlatillos();
        //bool AddPlatillo(PlatilloDTO platillo);
    }
}
