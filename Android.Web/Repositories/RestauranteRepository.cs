﻿using Android.Data;
using Android.Data.Database;
using Android.Web.Contracts;
using MySql.Data.MySqlClient;

namespace Android.Web.Repositories
{
    public class RestauranteRepository: IRestauranteRepository
    {
        private RestauranteDBContext Db { get; set; }
        //private readonly MySqlConnection Db;

        // public RestauranteRepository(MySqlConnection context) 
        public RestauranteRepository(RestauranteDBContext context)
        {
            Db = context; // new MySqlConnection(context.ConnectionString);
        }

        public IList<PlatilloDTO> GetPlatillos()
        {
            var respuesta = Db.Platillos?.Select(platillo => platillo.ToDTO()).ToList();
            return respuesta ?? new List<PlatilloDTO>();
        }

        /*public async Task<IList<PlatilloDTO>> GetPlatillos()
        {
            var results = new List<PlatilloDTO>();
            //return Db.Platillos.Select(p => p.ToDTO()).ToList();
            await Db.OpenAsync();

            using var command = new MySqlCommand("SELECT * FROM platillo;", Db);
            using var reader = await command.ExecuteReaderAsync();
            while (await reader.ReadAsync())
            {
                results.Add(new PlatilloDTO
                {
                    Id = reader.GetInt32(0),
                    Nombre = reader.GetValue(1).ToString()
                });
            }
            await reader.CloseAsync();
            return results;
        }*/

        /*public bool AddPlatillo(PlatilloDTO platillo)
        {
            try
            {
                Db.Platillos.Add(platillo.ToModel());
                Db.SaveChanges();
                return true;
            } catch(Exception)
            {
                return false;
            }
        }*/
    }
}
